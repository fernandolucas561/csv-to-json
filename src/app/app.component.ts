import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'csv-to-json';
  json = '';
  fileName = '';

  async handleFileInput(event: any) {
    const fileCsv = event.target.files.item(0);
    const dataCsv = await fileCsv.text();
    const dataObject = this.convertToObject(dataCsv);
    this.fileName = fileCsv.name.split('.')[0];
    this.json = JSON.stringify(dataObject, null, 2);
  }

  downloadJson() {
    if (this.json === '') {
      alert('Nenhum arquivo encontrado');
    } else {
      const download = document.createElement('a');
      download.setAttribute('href', 'data:application/json;charset=UTF-8,' + encodeURIComponent(this.json));
      download.setAttribute('download', this.fileName);
      download.click();
    }
  }

  convertToObject(content: string) {
    const lines = content.split('\n');

    const keyValueArray = lines.map(line => {
      const keyValueLine = line.split(';');
      const keys = keyValueLine[0].split('.');
      const value = keyValueLine[1].replace('\r', '');
      return [...keys, value];
    });
    
    const dataObjectArray: any = [];

    keyValueArray.forEach((item: string[]) => {
      const firstKey = item[0];
      const findObject = dataObjectArray.find((item: any) => item[firstKey]);

      if (findObject == null || findObject == undefined) {
        const objeto = item.reduceRight((previous: any, current: string) => {
          const builtedObject: any = {};
          builtedObject[current] = previous;
          return builtedObject;
        });

        dataObjectArray.push(objeto);
      } else {
        let findObjectChildren = findObject[firstKey];
        let isKeyChanged = false;

        item.forEach((element: string, indexItem: number) => {
          if ((element != firstKey || (element == firstKey && indexItem !=0 )) && !isKeyChanged) {
            if (findObjectChildren[element] === null || findObjectChildren[element] === undefined) {
              findObjectChildren[element] = item.reduceRight((previous: any, current: string, indexChildren: number) => {
                const currentObject: any = {};

                if ((current === element && indexItem === indexChildren) || isKeyChanged) {
                  isKeyChanged = true;
                  return previous;
                }
                
                currentObject[current] = previous;
                return currentObject;
              });

            } else {
              findObjectChildren = findObjectChildren[element];
            }
          }
        });
      }
    });

    const dataObject = this.arrayToObject(dataObjectArray);
    return dataObject;
  }

  arrayToObject(dataObjectArray: object[]) {
    const newObject: any = {};

    dataObjectArray.forEach((item: object) => {
      const key = Object.keys(item)[0];
      const values = Object.values(item)[0];
      newObject[key] = values;
    });

    return newObject;
  }
}
